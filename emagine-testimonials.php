<?php
/**
 * Plugin Name: Emagine - Testimonials
 * Plugin URI: https://bitbucket.org/hcwebdesign/emagine-testimonials
 * Description: A WordPress plugin for allowing editors to create Testimonials and anywhere inside of a page's content.
 * Version: 1.0
 * Author: Hunter Cox
 * Author URI: https://huntercox.work
 * License: GPL2
 * Emagine - Testimonials is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * Emagine - Testimonials is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Emagine - Testimonials.
 * If not, see https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html.
 *
 * @package WordPress
 */

defined( 'ABSPATH' ) || die( 'No script kiddies please!' );

/**
* Constants *
*/
	if ( ! defined( 'EMAGINE_PLUGIN' ) ) {
		define( 'EMAGINE_PLUGIN', __FILE__ );
	}
	if ( ! defined( 'EMAGINE_PLUGIN_DIR' ) ) {
		define( 'EMAGINE_PLUGIN_DIR', plugin_dir_path( EMAGINE_PLUGIN ) );
	}
	if ( ! defined( 'EMAGINE_PLUGIN_URL' ) ) {
		define( 'EMAGINE_PLUGIN_URL', plugin_dir_url( EMAGINE_PLUGIN ) );
	}

/**
* Autoload Classes *
*/
	spl_autoload_register( function ( $classname ) {
		$classname = strtolower( $classname );
		$classname = str_replace( '_', '-', $classname );
		$file_path = EMAGINE_PLUGIN_DIR . '/classes/class-' . $classname . '.php';
		if ( file_exists( $file_path ) ) {
			require_once $file_path;
		}
	});

$activate = new Activate;
$activate->setup_plugin();
