<?php
/**
 * Shortcode to display testimonial.
 *
 * Configure shortcode to accept parameters to display a specific testimonial.
 *
 * @since 1.0.0
 */

class Shortcode {



		public static function output_shortcode( $atts ) {
				$atts = shortcode_atts(
					array(
						'source' => 'random',
					), $atts, 'testimonial' );

				$param = $atts['source'];

				if ( 'random' == $param || '' == $param || null == $param ) {
					$args = array(
						'post_type'       => 'testimonial',
						'orderby'         => 'rand',
						'posts_per_page'  => '1',
					);

					$posts = get_posts( $args );

					foreach ( $posts as $post ) {
						$id      = $post->ID;
						$name    = $post->post_title;
						$content = $post->post_content;
						$content = wp_strip_all_tags( $content );
						$image   = get_the_post_thumbnail( $id, 'testimonial' );

						$output  = '
							<style type="text/css">
								.testimonial {
									display: flex;
									justify-content: flex-start;
								}
								.testimonial__image {
									flex-basis: 20%;
									margin-right: 20px;
								}
								.testimonial__content {
									flex-basis: 80%;
								}
								.testimonial__text {
									font-size: 20px;
									position: relative;
								}
								.testimonial__text p {
									margin-bottom: 8px;
									padding-right: 10%;
								}
								.testimonial__text p::after {
									content: "\f122";
									font-size: 54px;
									font-family: dashicons;
									position: absolute;
									top: -22px;
									right: -15px;
								}
								.testimonial__source {
									font-size: 18px;
								}
							</style>
							<div class="testimonial">
								<div class="testimonial__image">'.$image.'</div>
								<div class="testimonial__content">
									<div class="testimonial__text"><p>'.$content.'</p></div>
									<p class="testimonial__source"> - '.$name.'</p>
								</div>
							</div>
						';

						return $output;
					}




				} elseif ( $post = get_page_by_path( $param, OBJECT, 'testimonial' ) ) {

					$id      = $post->ID;
					$name    = $post->post_title;
					$content = $post->post_content;
					$content = wp_strip_all_tags( $content );
					$image   = get_the_post_thumbnail( $id, 'testimonial' );

					$output  = '
						<style type="text/css">
							.testimonial {
								display: flex;
								justify-content: flex-start;
							}
							.testimonial__image {
								flex-basis: 20%;
								margin-right: 20px;
							}
							.testimonial__content {
								flex-basis: 80%;
							}
							.testimonial__text {
								font-size: 20px;
								position: relative;
							}
							.testimonial__text p {
								margin-bottom: 8px;
								padding-right: 10%;
							}
							.testimonial__text p::after {
								content: "\f122";
								font-size: 54px;
								font-family: dashicons;
								position: absolute;
								top: -22px;
								right: -15px;
							}
							.testimonial__source {
								font-size: 18px;
							}
						</style>
						<div class="testimonial">
							<div class="testimonial__image">'.$image.'</div>
							<div class="testimonial__content">
								<div class="testimonial__text"><p>'.$content.'</p></div>
								<p class="testimonial__source">'.$name.'</p>
							</div>
						</div>
					';

					return $output;

				} else {

					return false;

				}

		}

		public static function register_testimonial_shortcode() {
				add_shortcode( 'testimonial', array( 'Shortcode', 'output_shortcode' ) );
		}

}
