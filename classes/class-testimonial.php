<?php
/**
 * Testimonials setup.
 *
 * Configuring Testimonials custom-post-type data.
 *
 * @since 1.0.0
 */

class Testimonial {

public $slug = null;

	/**
	 * Register custom post type.
	 *
	 * Add naming conventions and configuration settings for Testimonials custom post type.
	 *
	 * @since 1.0.0
	 *
	 * @see register_post_type()
	 * @link https://developer.wordpress.org/reference/functions/register_post_type/
	 * @package WordPress
	 */

			public static function register_testimonials() {
				$labels = array(
					'name'                  => _x( 'Testimonials', 'Post Type General Name' ),
					'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name' ),
					'menu_name'             => __( 'Testimonials' ),
					'name_admin_bar'        => __( 'Testimonial' ),
					'archives'              => __( 'Testimonial Archives' ),
					'attributes'            => __( 'Testimonial Attributes' ),
					'parent_item_colon'     => __( 'Parent Testimonial:' ),
					'all_items'             => __( 'All Testimonials' ),
					'add_new_item'          => __( 'Add New Testimonial' ),
					'add_new'               => __( 'Add New' ),
					'new_item'              => __( 'New Testimonial' ),
					'edit_item'             => __( 'Edit Testimonial' ),
					'update_item'           => __( 'Update Testimonial' ),
					'view_item'             => __( 'View Testimonial' ),
					'view_items'            => __( 'View Testimonials' ),
					'search_items'          => __( 'Search Testimonial' ),
					'not_found'             => __( 'Not found' ),
					'not_found_in_trash'    => __( 'Not found in Trash' ),
					'featured_image'        => __( 'Featured Image' ),
					'set_featured_image'    => __( 'Set featured image' ),
					'remove_featured_image' => __( 'Remove featured image' ),
					'use_featured_image'    => __( 'Use as featured image' ),
					'insert_into_item'      => __( 'Insert into Testimonial' ),
					'uploaded_to_this_item' => __( 'Uploaded to this testimonial' ),
					'items_list'            => __( 'Testimonials list' ),
					'items_list_navigation' => __( 'Testimonials list navigation' ),
					'filter_items_list'     => __( 'Filter testimonials list' ),
				);
				$args = array(
					'label'                 => __( 'Testimonial' ),
					'description'           => __( 'Testimonials for pages.' ),
					'labels'                => $labels,
					'supports'              => array( 'title', 'thumbnail', 'editor', 'revisions' ),
					'hierarchical'          => false,
					'public'                => true,
					'show_ui'               => true,
					'show_in_menu'          => true,
					'menu_position'         => 5,
					'show_in_admin_bar'     => true,
					'show_in_nav_menus'     => true,
					'can_export'            => false,
					'has_archive'           => true,
					'exclude_from_search'   => false,
					'publicly_queryable'    => true,
					'capability_type'       => 'post',
				);
				register_post_type( 'testimonial', $args );
			}


	/**
	 * Registers testimonials during init().
	 *
	 * Hooks the register_testimonials() method into init().
	 *
	 * @since 1.0.0
	 *
	 * @see add_action()
	 * @link https://developer.wordpress.org/reference/functions/add_action/
	 */

		public static function hook_testimonials_registration() {
			add_action( 'init', array( 'Testimonial', 'register_testimonials' ) );
		}


	/**
	 * Customize tinymce editor.
	 *
	 * Removes media button, toolbar and visual/text tab buttons.
	 *
	 * @since 1.0.0
	 *
	 * @see add_action()
	 * @link https://developer.wordpress.org/reference/functions/add_action/
	 */

			public static function remove_tinymce_media() {
					remove_action( 'media_buttons', 'media_buttons' );
			}


			public static function remove_tinymce_visual_tabs() {
					echo '<style type="text/css">
					        .mce-toolbar-grp, #wp-content-editor-tools, #post-status-info {
					          display:none;
					        }
									#wp-content-wrap {
										padding-top: 0 !important;
									}
									.mce-edit-area {
										padding-top: 0 !important;
									}
									.wp-editor-area {
										margin-top: 0 !important;
									}
									#tinymce {
										margin: 15px !important;
									}
					      </style>';
					echo '<script type="text/javascript">
					        jQuery(document).ready(function(){
					            jQuery("#content-tmce").attr("onclick", null);
					        });
					      </script>';
			}


			public static function add_tinymce_editor_heading() {
			  	echo '<h2>Quote</h2>';
			}


			public static function replace_edit_title_text() {
					$title = 'Enter name of source';

					return $title;
			}



	/**
	 * Testimonial image size.
	 *
	 * Creates a custom size to be used for testimonial featured images.
	 *
	 * @since 1.0.0
	 *
	 * @see add_image_size()
	 * @link https://developer.wordpress.org/reference/functions/add_image_size/
	 */

			public static function add_testimonial_image_size() {
					add_image_size( 'testimonial', 200, 200, true );
			}



	/**
	 * Setup shortcode data.
	 *
	 * Get the testimonial slug and pass it along for use as shortcode parameter.
	 *
	 * @since 1.0.0
	 *
	 * @see get_queried_object()
	 * @link https://developer.wordpress.org/reference/classes/wp_query/get_queried_object/
	 */

			public static function get_testimonial_slug() {
					$current_slug = get_queried_object()->slug;
					set_testimonial_slug( $current_slug );
			}


			public function set_testimonial_slug( $slug ) {
					$slug = $this->slug;

					return $slug;
			}




}
