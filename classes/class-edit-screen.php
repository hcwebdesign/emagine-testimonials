<?php
/**
 * Customize testimonial edit screen.
 *
 * Add inline CSS and Javascript, remove some php and change wording for testimonial context.
 *
 * @since 1.0.0
 */

class Edit_Screen {

		public static function customize_testimonials() {
				add_action( 'admin_head', array( 'Testimonial', 'remove_tinymce_media' ) );

				add_action( 'admin_footer', array( 'Testimonial', 'remove_tinymce_visual_tabs' ) );

				add_action( 'edit_form_after_title', array( 'Testimonial', 'add_tinymce_editor_heading' ) );

				add_filter( 'enter_title_here', array( 'Testimonial', 'replace_edit_title_text' ) );
		}


		public static function check_post_type() {
				$screen = get_current_screen();
				if ( 'testimonial' == $screen->post_type && 'testimonial' == $screen->id ) :
					Edit_Screen::customize_testimonials();
				endif;
		}


		public static function testimonial_edit_screen() {
				add_action( 'current_screen', array( 'Edit_Screen', 'check_post_type' ) );
		}


}
