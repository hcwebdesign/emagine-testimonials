<?php
/**
 * Handles plugin activation
 *
 * Sets up necessary data for WordPress admin when plugin is activated.
 *
 * @since 1.0.0
 */

class Activate {

		public function init() {
			register_activation_hook( EMAGINE_PLUGIN, function() {
				add_option( 'activated_plugin', 'emagine-testimonials-plugin' );
			});
		}


		public function remove_option() {
			if ( is_admin() && get_option( 'activated_plugin' ) == 'emagine-testimonials-plugin' ) {
				delete_option( 'activated_plugin' );
			}
		}


		public function setup_plugin() {
			add_action( 'admin_init', array( $this, 'remove_option' ) );
			add_action( 'init', array( 'Testimonial', 'add_testimonial_image_size' ) );

			Testimonial::hook_testimonials_registration();
			Edit_Screen::testimonial_edit_screen();
			Shortcode::register_testimonial_shortcode();
		}


}
