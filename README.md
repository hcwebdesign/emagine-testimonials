##
# Instructions from Emagine
##

WordPress Exercise

For the second exercise, we would like you to create a plugin that allows editors of a WordPress site the ability to include testimonials on any page. The requirements for this plugin are:

- Testimonial data should be managed via a Custom Post Type
- Editor should be allowed to easily add:
		 - the testimonial (quote)
		 - source (person who left the testimonial)
		 - associated image.
- The intended size of the associated image should be 200px x 200px
- Editor should be allowed to add a testimonial anywhere within their content
- Editor should be allowed to specify what testimonial is being displayed or allow for a random testimonial to be rendered.
- The design is not import to the exercise, we are going to be verifying functionality and code integrity.


Once you have built your plugin, either send us a link to the repo where your plugin resides or send us a zip. If you have any questions, please reach out to the person you’ve been in contact with at emagine.


Good luck!
